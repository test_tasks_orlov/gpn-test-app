﻿import { Component, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatInputModule } from '@angular/material/input';

import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users: User[];
	
	displayedColumns = ['position', 'do', 'field', 'ks', 'ku', 'date', 'value'];
	dataSource = new MatTableDataSource(ELEMENT_DATA);
	@ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
	@ViewChild(MatSort, {static:true}) sort: MatSort;
	
	public filterProduct = (value: string) => {
		this.dataSource.filter = value;
	}

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
    }
	
	applyFilter(filterValue: string) {
		filterValue = filterValue.trim();
		filterValue = filterValue.toLowerCase();
		this.dataSource.filter = filterValue;
	}
}

export interface Element {
	position: number;
	do: string;
	field: string;
	ks: string;
	ku: string;
	date: string;
	value: string;
}

const ELEMENT_DATA: Element[] = [
	{position: 1, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС1', ku: 'ГПА1.1', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 2, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС1', ku: 'ГПА1.2', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 3, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС1', ku: 'ГПА1.3', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 4, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС1', ku: 'ГПА1.4', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 5, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС1', ku: 'ГПА1.5', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 6, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС2', ku: 'ГПА2.1', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 7, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС2', ku: 'ГПА2.2', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 8, do: 'Газпромнефть-Ямал', field: 'Новопортовское', ks: 'КС2', ku: 'ГПА2.3', date: '08.07.2020 16:30:28', value: 'ГТД РАБОТА'},
	{position: 9, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-1', ku: 'Z-201', date: '08.07.2020 16:30:00', value: 'ГТД РАБОТА'},
	{position: 10, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-1', ku: 'Z-202', date: '08.07.2020 16:30:00', value: 'РЕМОНТ'},
	{position: 11, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-1', ku: 'K-1360', date: '08.07.2020 16:30:00', value: 'НОс'},
	{position: 12, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-2', ku: 'K-2360A', date: '08.07.2020 16:30:00', value: 'ХОЛОДНЫЙ РЕЗЕРВ'},
	{position: 13, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-2', ku: 'K-2360B', date: '08.07.2020 16:30:00', value: 'РЕМОНТ'},
	{position: 14, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-2', ku: 'K-2330', date: '08.07.2020 16:30:00', value: 'НОс'},
	{position: 15, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-3', ku: 'K-3360A', date: '08.07.2020 16:30:00', value: 'РЕМОНТ'},
	{position: 16, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-3', ku: 'K-3360B', date: '08.07.2020 16:30:00', value: 'ГТД РАБОТА'},
	{position: 17, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-3', ku: 'K-3330', date: '08.07.2020 16:30:00', value: 'АОс'},
	{position: 18, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-4', ku: 'K-4360A', date: '08.07.2020 16:30:00', value: 'РЕМОНТ'},
	{position: 19, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-4', ku: 'K-4360B', date: '08.07.2020 16:30:00', value: 'ПОДГОТОВКА ХР'},
	{position: 20, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-4', ku: 'K-4360C', date: '08.07.2020 16:30:00', value: 'ГТД РАБОТА'},
	{position: 21, do: 'Газпромнефть-Оренбург', field: 'ВУ ОНГКМ', ks: 'ТЛ-4', ku: 'K-4360D', date: '08.07.2020 16:30:00', value: 'ГТД РАБОТА'},
];