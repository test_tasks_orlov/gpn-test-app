export class Lightobjects {
  created: Date;
  updated: Date;
  name: string;
  ip: string;
  port: string;
  location: {
    lat: number,
    lng: number
  };
  address: string;
  model: string;
  active: boolean;
  id: number;
  zoneId: number;
}